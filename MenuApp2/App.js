/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import { StyleSheet, Text, View} from 'react-native';
import { createBottomTabNavigator } from 'react-navigation';
import HomeScreen from './screens/HomeScreen';
import OrderScreen from './screens/OrderScreen';
import AppNavigator from './navigation/AppNavigator';

type Props = {};

const TabNavigator = createBottomTabNavigator({
  Menu: HomeScreen,
  Order: OrderScreen,
});


export default class App extends Component<Props> {
  render() {
    return (
      <AppNavigator />
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

import React from 'react';
import { View, Image, Text, StyleSheet, TouchableOpacity, Alert, TextInput } from 'react-native';
import foodPic from '../assets/images/foodimages/Stoofvlees.png';
import LargeButton from '../components/LargeButton';

export default class SettingsScreen extends React.Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam('text', 'Item'),
    }
  };

  constructor(props){
    super(props);
      this.state={
        items: 0,
        price: 15,
        total: 0,
      };
  }

  AddToOrder = () => {
    if(this.state.items == 0 || this.state.total == 0){
      Alert.alert(
        'Kies een aantal',
        'Gelieve minstens 1 item te kiezen.',
        [
            { text: 'OK', onPress: () => console.log('OK Pressed') },
        ],
        { cancelable: false },
      );
    } else {
    Alert.alert(
      'Item toegevoegd',
      'Item werd toegevoegd aan bestelling.',
      [
          { text: 'OK', onPress: () => console.log('OK Pressed') },
      ],
      { cancelable: false },
    );
    }
  }

  removeItem = () => {
    if(this.state.items > 0){
      this.setState({
        items: this.state.items-1,
        total: this.state.total-this.state.price
      })
    }
  }

  addItem = () => {
    this.setState({
      items: this.state.items+1,
      total: this.state.total+this.state.price
    })
  }

  render() {
    /* Go ahead and delete ExpoConfigView and replace it with your
     * content, we just wanted to give you a quick view of your config */
    return (
      <View style={styles.Container}>
        <View style={styles.Container}>
          <Image style={styles.Image} source={foodPic} />
          <View style={styles.MainInfo}>
            <Text style={styles.ItemInfoText}>Stoofvlees met frietjes</Text>
            <Text style={styles.ItemPrice}>€15.00</Text>
          </View>
          <View style={styles.Desc}>
            <Text style={styles.Desc}>
              Vers stoofvlees op Vlaamse wijze.
              Wordt geserveerd met Frietjes
              en een salade
            </Text>
          </View>
          <View style={styles.Comment}>
            <Text style={styles.CommentTitle}>
              Opmerkingen
            </Text>
            <View style={styles.CommentInput}>
            <TextInput
              multiline = {true}
              numberOfLines = {4}
              onChangeText={(text) => this.setState({text})}
              value={this.state.text}
              editable = {true}
              maxLength = {40}
            />
            </View>
          </View>
        </View>
        <View style={styles.OrderOptions}>
          <View style={styles.ItemTotal}>
              <TouchableOpacity style={styles.RemoveButton} onPress={this.removeItem}>
                <Text style={styles.AmountRemove}>-</Text>
              </TouchableOpacity>
              <Text style={styles.Amount}>{this.state.items}</Text>
              <TouchableOpacity style={styles.AddButton}  onPress={this.addItem}>
                <Text style={styles.AmountAdd}>+</Text>
              </TouchableOpacity>
          </View>
          <LargeButton
              buttonText={"Voeg toe aan bestelling - "+ this.state.total.toFixed(2)}
              onClick={this.AddToOrder}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  Container: {
    flex: 1,
  },
  Image: {
    width: '100%',
    height: 200
  },
  MainInfo: {
    width: '100%',
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    paddingLeft: 5,
    paddingRight: 5,
  },
  ItemInfoText: {
    fontSize: 25,
    alignItems: 'flex-start',
    fontWeight: 'bold',
  },
  ItemPrice: {
    fontSize: 25,
    color: 'blue',
    alignItems: 'center',
    fontWeight: 'bold',
  },
  Desc: {
    alignItems: 'flex-start',
    paddingLeft: 5,
    paddingTop: 5,
    width: '90%',
  },
  Comment: {
    alignItems: 'center',
    paddingLeft: 5,
    paddingRight: 5,
  },
  CommentTitle: {
    fontSize: 17,
    fontWeight: 'bold',
    marginTop: 20,
    paddingLeft: 5,
    alignSelf: 'flex-start',
  },
  CommentInput: {
    borderColor: '#000000',
    borderWidth: 1,
    height: 100,
    width: '100%',
  },
  OrderOptions: {
    alignSelf: 'flex-end',
    alignItems: 'flex-end',
    width: '100%',
    alignItems: 'center',
    backgroundColor: '#F9F9F9',
},
ItemTotal: {
  width: '95%',
  flexDirection: 'row',
  justifyContent: 'space-evenly',
  marginLeft: 20,
  marginRight: 20,
  marginBottom: 10,
  marginTop: 10,
},
Amount: {
  fontSize: 18,
  fontWeight: 'bold',
},
AmountRemove: {
  fontSize: 18,
  color: 'red',
},
AmountAdd: {
  fontSize: 20,
  color: 'green',
},
RemoveButton: {
  width: 30,
  height: 30,
  borderRadius: 360,
  borderWidth: 2.5,
  borderColor: 'red',
  alignItems: 'center',
  justifyContent: 'center',
},
AddButton: {
  width: 30,
  height: 30,
  borderRadius: 360,
  borderWidth: 2.5,
  borderColor: 'green',
  alignItems: 'center',
  justifyContent: 'center',
}, 


})

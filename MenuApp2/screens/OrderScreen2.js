import React from 'react';
import { StyleSheet, ScrollView, View, Text, Alert } from 'react-native';
import OrderItem from '../components/OrderItem';
import LargeButton from '../components/LargeButton';

export default class OrderScreen extends React.Component {
    static navigationOptions = ({ navigation }) => {
        return {
            title: "Order: 81988",
        }
    };
    constructor(props){
        super(props);

        this.state ={
            orderItems: [
                { amount: 1, title: 'Stoofvlees', subtitle: 'test test hey', price: 15 },
                { amount: 1, title: 'Couscous', subtitle: 'Vlaamse wijze', price: 25 },
                { amount: 1, title: 'Apple Pie', subtitle: 'Verse appels', price: 8 },
            ],
        }
    }

    ShowOrderReview = (id) => {
        //searchID;
        Alert.alert(
            'Bestelling geplaatst',
            'Uw bestelling voor tafel '+id+' werd goed ontvangen.',
            [
                { text: 'OK', onPress: () => console.log('OK Pressed') },
            ],
            { cancelable: false },
        );
    }

    getList() {
        
    }

    render() {
        
        return (
            <View style={{ flex: 1 }}>
                <ScrollView contentContainerStyle={styles.contentContainer}>
                    {
                        this.state.orderItems.map((item, key) =>{
                            return  <OrderItem
                                key={key}
                                amount={item.amount}
                                title={item.title}
                                subtitle={item.subtitle}
                                price={item.price}
                            />
                        })
                    }
                    
                </ScrollView>
                <View style={styles.OrderOptions}>
                    <View style={styles.ItemTotal}>
                        <Text style={styles.TotalText}>Totaalbedrag:</Text>
                        <Text style={styles.TotalText}>€25.00</Text>
                    </View>
                    <LargeButton
                        buttonText={"BESTEL"}
                        onClick={() => this.ShowOrderReview("987628")}
                    />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    contentContainer: {
        flex: 1,
        marginTop: 15,
        marginLeft: 10,
        marginRight: 10,
    },
    OrderOptions: {
        width: '100%',
        alignItems: 'center',
        backgroundColor: '#F9F9F9',
    },
    ItemTotal: {
        width: '95%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginLeft: 20,
        marginRight: 20,
        marginBottom: 10,
        marginTop: 10,
    },
    TotalText: {
        fontSize: 18,
    },
})

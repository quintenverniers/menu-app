import React from 'react';
import { StyleSheet, ScrollView, View, TouchableOpacity, Image, Text } from 'react-native';
import Suggestion from '../components/SuggestionItem';
import FoodItem from '../components/FoodItem';
import foodPic from '../assets/images/foodimages/Stoofvlees.png';

export default class CategoryScreen extends React.Component {
    static navigationOptions = ({ navigation }) => {
        return {
            title: navigation.getParam('text', 'Category'),
        }
    };

    addToAsyncArray = () => {
        alert("Item added");
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <ScrollView contentContainerStyle={styles.contentContainer}>
                    <View style={styles.Suggestions}>
                        <Text style={styles.Title}>Suggesties</Text>
                        <Suggestion
                            onClick={() => this.props.navigation.navigate('ItemView', { text: 'Stoofvlees'})}
                            title={"Stoofvlees Friet"}
                            price={"15,00"}
                            desc={"Vers stoofvlees op Vlaamse wijze"}
                            img={foodPic}
                            onAdd={this.addToAsyncArray}
                        />
                        <Suggestion
                            onClick={() => this.props.navigation.navigate('ItemView', { text: 'Stoofvlees' })}
                            title={"Couscous Maison"}
                            price={"15,00"}
                            desc={"Couscous met Belgische Twist"}
                            img={foodPic}
                            onAdd={this.addToAsyncArray}
                        />
                    </View>
                    <View style={styles.Category}>
                        <Text style={styles.Title}>Vleesgerechten</Text>
                        <FoodItem
                            onClick={() => this.props.navigation.navigate('ItemView', { text: 'Stoofvlees' })}
                            title={"Stoofvlees Friet"}
                            price={"15,00"}
                            desc={"Vers stoofvlees op Vlaamse wijze"}
                            img={foodPic}
                            onAdd={this.addToAsyncArray}
                        />
                        <FoodItem
                            onClick={() => this.props.navigation.navigate('ItemView', { text: 'Stoofvlees' })}
                            title={"Stoofvlees Friet"}
                            price={"15,00"}
                            desc={"Vers stoofvlees op Vlaamse wijze"}
                            img={foodPic}
                            onAdd={this.addToAsyncArray}
                        />
                        <FoodItem
                            onClick={() => this.props.navigation.navigate('ItemView', { text: 'Stoofvlees' })}
                            title={"Stoofvlees Friet"}
                            price={"15,00"}
                            desc={"Vers stoofvlees op Vlaamse wijze"}
                            img={foodPic}
                            onAdd={this.addToAsyncArray}
                        />
                        <FoodItem
                            onClick={() => this.props.navigation.navigate('ItemView', { text: 'Stoofvlees' })}
                            title={"Stoofvlees Friet"}
                            price={"15,00"}
                            desc={"Vers stoofvlees op Vlaamse wijze"}
                            img={foodPic}
                            onAdd={this.addToAsyncArray}
                        />
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    contentContainer: {
        marginTop: 15,
        alignItems: 'center',
    },
    Suggestions: {
        marginBottom: 30,
    },
    Category: {
        marginBottom: 30,
    },
    Title: {
        fontSize: 30,
        alignSelf: 'flex-start',
        marginLeft: 10,
        fontWeight: 'bold',
        marginBottom: 5,
    },
})

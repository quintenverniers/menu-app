import React from 'react';
import { ScrollView, StyleSheet, TouchableOpacity, View, Text, ImageBackground } from 'react-native';
import CategoryButton from '../components/CategoryButton';
import Burger from '../assets/images/Category-Images/Burger.png';
import Soup from '../assets/images/Category-Images/Soup.png';
import Cupcake from '../assets/images/Category-Images/Cupcake.png';
import Cola from '../assets/images/Category-Images/Cola.png';

export default class LinksScreen extends React.Component {
  static navigationOptions = {
    title: 'Menu',
  };

  componentDidMount(){
    
  }

  render() {
    return (
      <View style={{ flex: 1}}>
      <ScrollView style={styles.container} contentContainerStyle={styles.contentContainer}>
        <CategoryButton 
          onClick={() => this.props.navigation.navigate('CategoryView', {text: 'Hoofdgerechten'})}
          title={"Hoofdgerechten"}
          img={Burger}
        />
        <CategoryButton 
          onClick={() => this.props.navigation.navigate('CategoryView', {text: 'Voorgerechten'})}
          title={"Voorgerechten"}
          img={Soup}
        />
        <CategoryButton 
          onClick={() => this.props.navigation.navigate('CategoryView', {text: 'Desserten'})}
          title={"Desserten"}
          img={Cupcake}
        />
        <CategoryButton 
          onClick={() => this.props.navigation.navigate('CategoryView', {text: 'Dranken'})}
          title={"Dranken"}
          img={Cola}
        />
      </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
    backgroundColor: '#fff',
  },
  contentContainer: {
    alignItems: 'center'
  },
});

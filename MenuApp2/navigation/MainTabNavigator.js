import React from 'react';
import { Platform } from 'react-native';
import { createStackNavigator, createBottomTabNavigator } from 'react-navigation';
import Icon from '../components/Icon';
import Basket from '../assets/images/icons8-basket-filled-50.png';
import Knife from '../assets/images/icons8-dining-room-50.png';
import Beacons from '../assets/images/icons8-ibeacon-filled-50.png';

import OrderScreen from '../screens/OrderScreen';
import BeaconScreen from '../screens/OrderScreenBeacons';
import HomeScreen from '../screens/HomeScreen';
import CategoryScreen from '../screens/CategoryScreen';
import ItemScreen from '../screens/ItemScreen';

const HomeStack = createStackNavigator({
  Home: HomeScreen,
  CategoryView: CategoryScreen,
  ItemView: ItemScreen,
});

HomeStack.navigationOptions = {
  tabBarLabel: 'Menu',
  tabBarIcon: () => (
    <Icon
      source={Knife}
    />
  )
};

const OrdersStack = createStackNavigator({
  Settings: OrderScreen,
});

OrdersStack.navigationOptions = {
  tabBarLabel: 'Bestelling',
  tabBarIcon: () => (
    <Icon
      source={Basket}
    />
  )
};

const BeaconStack = createStackNavigator({
    Beacons: BeaconScreen,
});

BeaconStack.navigationOptions = {
    tabBarLabel: 'Beacons',
    tabBarIcon: () => (
        <Icon
            source={Beacons}
        />
    )
};

export default createBottomTabNavigator({
  HomeStack,
  OrdersStack,
  BeaconStack
});

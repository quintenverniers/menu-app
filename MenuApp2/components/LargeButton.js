import React from 'react';
import { StyleSheet, TouchableOpacity, Text} from 'react-native';

const LargeButton = (props) => {
    return(
        <TouchableOpacity style={styles.Button} onPress={props.onClick}>
            <Text style={styles.ButtonText}>{props.buttonText}</Text>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    Button: {
        backgroundColor: '#4BF952',
        width: '95%',
        alignItems: 'center',
        justifyContent: 'center',
        height: 40,
        marginBottom: 15,
        borderRadius: 5,
    },
    ButtonText: {
        color: 'white',
        fontSize: 18,
        fontWeight: 'bold',
    },
})


export default LargeButton;
import React from 'react';
import { StyleSheet, TouchableOpacity, View, Text, ImageBackground } from 'react-native';

const CategoryButton = (props) => {
    return(
        <TouchableOpacity style={{borderRadius: 8}} onPress={props.onClick}>
            <View style={styles.CategoryItem}>
              <ImageBackground source={props.img} style={styles.ItemImage}>
              <Text style={styles.ItemText}>
                {props.title}
              </Text>
            </ImageBackground>
            </View>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    CategoryItem: {
        height: 130,
        minWidth: '95%',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 8,
        marginBottom: 10,
      },
      ItemImage: {
        width: '100%',
        height: '100%',
        borderRadius: 8,
        alignItems: 'center',
        justifyContent: 'center',
        overflow: 'hidden',
      },
      ItemText: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 35,
      },
})


export default CategoryButton;
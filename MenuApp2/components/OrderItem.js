import React from 'react';
import { StyleSheet, TouchableOpacity, View, Text } from 'react-native';

const OrderItem = (props) => {
    return(
        <View style={styles.OrderItem}>
            <View style={styles.ItemAmount}>
                <Text>{props.amount}x</Text>
            </View>
            <View style={styles.ItemInfo}>
                <Text style={styles.ItemTitle}>{props.title}</Text>
                <Text>{props.subtitle}</Text>
            </View>
            <View style={styles.ItemPriceContainer}>
                <Text style={styles.ItemPrice}>€{parseFloat(Math.round(props.price * 100) / 100).toFixed(2)}</Text>
            </View>
            <TouchableOpacity style={styles.DeleteItem}>
                <Text style={styles.Delete}>-</Text>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    OrderItem : {
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: '#F9F9F9',
        height: 75,
        borderRadius: 5,
        overflow: 'hidden',
        marginTop: 5,
        marginBottom: 5,
    },
    ItemAmount : {
        height: '100%',
        width: 40,
        alignItems: 'center',
        justifyContent: 'center',
    },
    ItemInfo: {
        flex: 1,
        justifyContent: 'flex-start',
        marginLeft: 10,
    },
    ItemTitle: {
        fontSize: 20,
        fontWeight: '600',
    },
    ItemPriceContainer: {
        flex: 1,
        alignItems: 'flex-end',
        marginRight: 10,
    },
    ItemPrice: {
        fontSize: 20,
        fontWeight: '600',
        color: 'blue',
    },
    DeleteItem: {
        backgroundColor: 'red',
        height: '100%',
        width: 50,
        alignItems: 'center',
        justifyContent: 'center',
    },
    Delete: {
        color: 'white',
        fontSize: 30,
        fontWeight: 'bold',
    }
})


export default OrderItem;
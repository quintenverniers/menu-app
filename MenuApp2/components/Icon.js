import React from 'react';
import { StyleSheet, Image } from 'react-native';


const Icon = (props) => {
    return(
        <Image 
            style={{width: 24, height: 24}}
            source={props.source}
        />
    )
}

const styles = StyleSheet.create({
    CategoryItem: {
        height: 130,
        minWidth: '95%',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 8,
        marginBottom: 10,
      },
      ItemImage: {
        width: '100%',
        height: '100%',
        borderRadius: 8,
        alignItems: 'center',
        justifyContent: 'center',
        overflow: 'hidden',
      },
      ItemText: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 35,
      },
})


export default Icon;
import React from 'react';
import { StyleSheet, TouchableOpacity, View, Text, ImageBackground } from 'react-native';

const Suggestion = (props) => {
    return(
        <TouchableOpacity onPress={props.onClick}>
        <View style={styles.SuggestionItem}>
                <ImageBackground source={{uri: "http://chefsproveggie.be/assets/files/imager/assets/files/recipes/309/stoofvlees_e1465214d4a07f4220fec000729cb738.png"}} style={styles.ItemImage}>
                    <View style={styles.ItemInfo}>
                        <Text style={styles.ItemInfoText}>{props.title}</Text>
                        <Text style={styles.ItemPrice}>€{props.price}</Text>
                    </View>
                </ImageBackground>
            <View style={styles.ItemDetails}>
                <Text style={styles.ItemDetailText}>{props.desc}.</Text>
                <TouchableOpacity onPress={props.onAdd}>
                    <View style={styles.AddButton}>
                        <Text style={styles.AddButtonText}>
                            +
                        </Text>
                    </View>
                </TouchableOpacity>
            </View>
        </View>
    </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    SuggestionItem: {
        backgroundColor: '#F9F9F9',
        height: 200,
        minWidth: '95%',
        borderRadius: 8,
    },
    ItemImage: {
        width: '100%',
        height: 150,
        borderTopLeftRadius: 8, 
        borderTopRightRadius: 8,
        justifyContent: 'flex-end',
        overflow: 'hidden',
    },
    ItemInfo: {
        height: 35,
        backgroundColor: 'rgba(70, 70, 70, 0.6)',
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row',
        paddingLeft: 5,
        paddingRight: 5,
    },
    ItemInfoText: {
        fontSize: 20,
        color: 'white',
        alignItems: 'flex-start',
        fontWeight: 'bold',
    },
    ItemPrice: {
        fontSize: 20,
        color: 'white',
        alignItems: 'center',
        fontWeight: 'bold',
    },
    ItemDetails: {
        flex: 1,
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row',
        paddingLeft: 5,
        paddingRight: 5,
    },
    ItemDetailText: {
    },
    AddButton: {
        backgroundColor: '#4BF952',
        height: 35,
        width: 55,
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center',
    },
    AddButtonText: {
        fontSize: 25,
        fontWeight: 'bold',
        color: '#FFF',
    }
});

export default Suggestion;
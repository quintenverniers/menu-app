import React from 'react';
import { StyleSheet, TouchableOpacity, View, Text, Image } from 'react-native';


const FoodItem = (props) => {
    return(
        <TouchableOpacity onPress={props.onClick}>
            <View style={styles.CategoryItem}>
                <Image source={props.img} style={styles.ItemImage}/>
                <View style={styles.ItemDetails}>
                    <Text style={styles.ItemTitle}>{props.title}</Text>
                    <Text style={styles.ItemDesc}>{props.desc.substring(0,25)}...</Text>
                    <Text style={styles.ItemPrice}>€{props.price}</Text>
                </View>
                <TouchableOpacity style={styles.ButtonWrapper} onPress={props.onAdd}>
                    <View style={styles.AddButton}>
                        <Text style={styles.AddButtonText}>
                            +
                        </Text>
                    </View>
                </TouchableOpacity>
            </View>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    CategoryItem: {
        marginBottom: 10,
        backgroundColor: '#F9F9F9',
        height: 80,
        minWidth: '95%',
        borderRadius: 8,
        flexDirection: 'row',
        alignItems: 'center',
    },
    ItemImage: {
        height: 70,
        width: 70,
        marginLeft: 5,
        borderRadius: 5,
    },
    ItemDetails: {
        marginLeft: 10,
    },
    ItemTitle: {
        fontSize: 20,
        fontWeight: 'bold',
    },
    ItemDesc: {

    },
    ItemPrice: {
        color: 'blue',
        fontWeight: '700',
    },
    ButtonWrapper: {
        height: '100%',
        width: 50,
        backgroundColor: '#4BF952',
        borderTopRightRadius: 5,
        borderBottomRightRadius: 5,
        position: 'absolute', right: 0
    },
    AddButton: {
        alignItems: 'center',
        justifyContent: 'center',
        height: '100%',
    },
    AddButtonText: {
        color: '#fff',
    }
})


export default FoodItem;